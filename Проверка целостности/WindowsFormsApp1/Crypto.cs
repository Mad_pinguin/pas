﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.IO;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

//using System.Timers;
namespace WindowsFormsApp1
{
    class Crypto
    {
        public static SHA256 Sha256 = SHA256.Create();

        // Compute the file's hash.
        private static byte[] GetHashSha256(string filename)
        {
            using (FileStream stream = File.OpenRead(filename))
            //                                                    2,147,483,647
            //                                                    1,000,000,000
            //                                                    1,073,741,824

            //using (FileStream stream = new FileStream(filename, FileMode.Open)
            //using (var stream = new BufferedStream(File.OpenRead(filename), 1073741824))
            {
                return Sha256.ComputeHash(stream);
            }
        }

        // Return a byte array as a sequence of hex values.
        private static string BytesToString(byte[] bytes)
        {
            string result = "";
            foreach (byte b in bytes) result += b.ToString("x2");
            return result;
        }
        
        public static string ReturninHash(string filename)
        {
            string HashString;
            try
            {
                if (!File.Exists(filename))
                    throw new FileNotFoundException();
            }
            catch (FileNotFoundException e)
            {
                MessageBox.Show("Файл не найден"); // your message here.
                return "";
            }
            HashString = BytesToString(GetHashSha256(filename));
            return HashString;
        }


        /*
        static System.Windows.Forms.Timer myTimer = new System.Windows.Forms.Timer();
        static int alarmCounter = 1;
        static bool exitFlag = false;
        private static void TimerEventProcessor(Object myObject,
                                            EventArgs myEventArgs)
        {
            myTimer.Stop();

            // Displays a message box asking whether to continue running the timer.
            if (MessageBox.Show("Continue running?", "Count is: " + alarmCounter,
               MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                // Restarts the timer and increments the counter.
                alarmCounter += 1;
                myTimer.Enabled = true;
            }
            else
            {
                // Stops the timer.
                exitFlag = true;
            }
        }

        // Adds the event and the event handler for the method that will process the timer event to the timer. 
        myTimer.Tick += new EventHandler(TimerEventProcessor);

        // Sets the timer interval to 5 seconds.
        myTimer.Interval = 5000;
       myTimer.Start();
 
       // Runs the timer, and raises the event.
       while(exitFlag == false) {
          // Processes all the events in the queue.
          Application.DoEvents();
       }*/

        /*
        public static void TimerStart()
        {
            SetTimer();
        }

        private static System.Timers.Timer aTimer; //timer

        private static void OnTimedEvent(Object source, ElapsedEventArgs e)
        {
            MessageBox.Show("Нарушение целостности!", (e.SignalTime).ToString());
        }

        public static void TimerStop()
        {
            aTimer.Stop();
            aTimer.Dispose();
        }

        private static void SetTimer()
        {
            // Create a timer with a two second interval.
            aTimer = new System.Timers.Timer(2000);
            // Hook up the Elapsed event for the timer. 
            aTimer.Elapsed += OnTimedEvent;
            aTimer.AutoReset = true;
            aTimer.Enabled = true;
        }
        */
    }
}
