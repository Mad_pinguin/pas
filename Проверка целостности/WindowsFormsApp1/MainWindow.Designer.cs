﻿
namespace WindowsFormsApp1
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.fileButton = new System.Windows.Forms.Button();
            this.folderButton = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.filesListBox = new System.Windows.Forms.ListBox();
            this.filesContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.DeletionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label1 = new System.Windows.Forms.Label();
            this.foldersListBox = new System.Windows.Forms.ListBox();
            this.foldersContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.folderDeletionToolStripButton = new System.Windows.Forms.ToolStripMenuItem();
            this.folderRemakeToolStripButton = new System.Windows.Forms.ToolStripMenuItem();
            this.label2 = new System.Windows.Forms.Label();
            this.tmpFilesListbox = new System.Windows.Forms.ListBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tmpFilesContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.добавитьФайлНаКонтрольЦелостностиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.игнорироватьДанныйФайлToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.failedFilesListBox = new System.Windows.Forms.ListBox();
            this.label4 = new System.Windows.Forms.Label();
            this.failedFilesContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.обновитьХешФайлаToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.удалитьФайлСКонтроляЦелостностиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tmpFoldersListbox = new System.Windows.Forms.ListBox();
            this.filesContextMenu.SuspendLayout();
            this.foldersContextMenu.SuspendLayout();
            this.tmpFilesContextMenuStrip.SuspendLayout();
            this.failedFilesContextMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // fileButton
            // 
            this.fileButton.Location = new System.Drawing.Point(12, 358);
            this.fileButton.Name = "fileButton";
            this.fileButton.Size = new System.Drawing.Size(107, 80);
            this.fileButton.TabIndex = 0;
            this.fileButton.Text = "Файл";
            this.fileButton.UseVisualStyleBackColor = true;
            this.fileButton.Click += new System.EventHandler(this.button1_Click);
            // 
            // folderButton
            // 
            this.folderButton.Location = new System.Drawing.Point(137, 358);
            this.folderButton.Name = "folderButton";
            this.folderButton.Size = new System.Drawing.Size(105, 80);
            this.folderButton.TabIndex = 1;
            this.folderButton.Text = "Папка";
            this.folderButton.UseVisualStyleBackColor = true;
            this.folderButton.Click += new System.EventHandler(this.button2_Click);
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // filesListBox
            // 
            this.filesListBox.FormattingEnabled = true;
            this.filesListBox.HorizontalScrollbar = true;
            this.filesListBox.Location = new System.Drawing.Point(12, 192);
            this.filesListBox.Name = "filesListBox";
            this.filesListBox.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.filesListBox.Size = new System.Drawing.Size(352, 160);
            this.filesListBox.TabIndex = 4;
            // 
            // filesContextMenu
            // 
            this.filesContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.DeletionToolStripMenuItem});
            this.filesContextMenu.Name = "contextMenuStrip1";
            this.filesContextMenu.Size = new System.Drawing.Size(369, 26);
            // 
            // DeletionToolStripMenuItem
            // 
            this.DeletionToolStripMenuItem.Name = "DeletionToolStripMenuItem";
            this.DeletionToolStripMenuItem.Size = new System.Drawing.Size(368, 22);
            this.DeletionToolStripMenuItem.Text = "Удалить выделенные файлы с контроля целостности";
            this.DeletionToolStripMenuItem.Click += new System.EventHandler(this.jjToolStripMenuItem_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 178);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(176, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Файлы на контроле целостности";
            // 
            // foldersListBox
            // 
            this.foldersListBox.FormattingEnabled = true;
            this.foldersListBox.HorizontalScrollbar = true;
            this.foldersListBox.Location = new System.Drawing.Point(12, 28);
            this.foldersListBox.Name = "foldersListBox";
            this.foldersListBox.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.foldersListBox.Size = new System.Drawing.Size(352, 147);
            this.foldersListBox.TabIndex = 7;
            // 
            // foldersContextMenu
            // 
            this.foldersContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.folderDeletionToolStripButton,
            this.folderRemakeToolStripButton});
            this.foldersContextMenu.Name = "foldersContextMenu";
            this.foldersContextMenu.Size = new System.Drawing.Size(364, 48);
            // 
            // folderDeletionToolStripButton
            // 
            this.folderDeletionToolStripButton.Name = "folderDeletionToolStripButton";
            this.folderDeletionToolStripButton.Size = new System.Drawing.Size(363, 22);
            this.folderDeletionToolStripButton.Text = "Удалить выделенные папки с контроля целостности";
            this.folderDeletionToolStripButton.Click += new System.EventHandler(this.удалитьВыделенныеФайлыСКонтроляЦелостностиToolStripMenuItem_Click);
            // 
            // folderRemakeToolStripButton
            // 
            this.folderRemakeToolStripButton.Name = "folderRemakeToolStripButton";
            this.folderRemakeToolStripButton.Size = new System.Drawing.Size(363, 22);
            this.folderRemakeToolStripButton.Text = "Обновить хеш файлов в папке";
            this.folderRemakeToolStripButton.Click += new System.EventHandler(this.обновитьХешФайловВПапкеToolStripMenuItem_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(171, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Папки на контроле целостности";
            // 
            // tmpFilesListbox
            // 
            this.tmpFilesListbox.FormattingEnabled = true;
            this.tmpFilesListbox.HorizontalScrollbar = true;
            this.tmpFilesListbox.Location = new System.Drawing.Point(370, 28);
            this.tmpFilesListbox.Name = "tmpFilesListbox";
            this.tmpFilesListbox.Size = new System.Drawing.Size(352, 147);
            this.tmpFilesListbox.TabIndex = 10;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(367, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(306, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Новые файлы в папках, стоящих на контроле целостности";
            // 
            // tmpFilesContextMenuStrip
            // 
            this.tmpFilesContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.добавитьФайлНаКонтрольЦелостностиToolStripMenuItem,
            this.игнорироватьДанныйФайлToolStripMenuItem});
            this.tmpFilesContextMenuStrip.Name = "tmpFilesContextMenuStrip";
            this.tmpFilesContextMenuStrip.Size = new System.Drawing.Size(303, 48);
            // 
            // добавитьФайлНаКонтрольЦелостностиToolStripMenuItem
            // 
            this.добавитьФайлНаКонтрольЦелостностиToolStripMenuItem.Name = "добавитьФайлНаКонтрольЦелостностиToolStripMenuItem";
            this.добавитьФайлНаКонтрольЦелостностиToolStripMenuItem.Size = new System.Drawing.Size(302, 22);
            this.добавитьФайлНаКонтрольЦелостностиToolStripMenuItem.Text = "Добавить файл на контроль целостности";
            this.добавитьФайлНаКонтрольЦелостностиToolStripMenuItem.Click += new System.EventHandler(this.добавитьФайлНаКонтрольЦелостностиToolStripMenuItem_Click);
            // 
            // игнорироватьДанныйФайлToolStripMenuItem
            // 
            this.игнорироватьДанныйФайлToolStripMenuItem.Name = "игнорироватьДанныйФайлToolStripMenuItem";
            this.игнорироватьДанныйФайлToolStripMenuItem.Size = new System.Drawing.Size(302, 22);
            this.игнорироватьДанныйФайлToolStripMenuItem.Text = "Игнорировать данный файл";
            this.игнорироватьДанныйФайлToolStripMenuItem.Click += new System.EventHandler(this.игнорироватьДанныйФайлToolStripMenuItem_Click);
            // 
            // failedFilesListBox
            // 
            this.failedFilesListBox.FormattingEnabled = true;
            this.failedFilesListBox.Location = new System.Drawing.Point(370, 194);
            this.failedFilesListBox.Name = "failedFilesListBox";
            this.failedFilesListBox.Size = new System.Drawing.Size(352, 160);
            this.failedFilesListBox.TabIndex = 12;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(367, 178);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(246, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "Файлы с проваленной проверкой целостности";
            // 
            // failedFilesContextMenu
            // 
            this.failedFilesContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.обновитьХешФайлаToolStripMenuItem1,
            this.удалитьФайлСКонтроляЦелостностиToolStripMenuItem});
            this.failedFilesContextMenu.Name = "failedFilesContextMenu";
            this.failedFilesContextMenu.Size = new System.Drawing.Size(288, 48);
            // 
            // обновитьХешФайлаToolStripMenuItem1
            // 
            this.обновитьХешФайлаToolStripMenuItem1.Name = "обновитьХешФайлаToolStripMenuItem1";
            this.обновитьХешФайлаToolStripMenuItem1.Size = new System.Drawing.Size(287, 22);
            this.обновитьХешФайлаToolStripMenuItem1.Text = "Обновить хеш файла";
            this.обновитьХешФайлаToolStripMenuItem1.Click += new System.EventHandler(this.обновитьХешФайлаToolStripMenuItem1_Click);
            // 
            // удалитьФайлСКонтроляЦелостностиToolStripMenuItem
            // 
            this.удалитьФайлСКонтроляЦелостностиToolStripMenuItem.Name = "удалитьФайлСКонтроляЦелостностиToolStripMenuItem";
            this.удалитьФайлСКонтроляЦелостностиToolStripMenuItem.Size = new System.Drawing.Size(287, 22);
            this.удалитьФайлСКонтроляЦелостностиToolStripMenuItem.Text = "Удалить файл с контроля целостности";
            this.удалитьФайлСКонтроляЦелостностиToolStripMenuItem.Click += new System.EventHandler(this.удалитьФайлСКонтроляЦелостностиToolStripMenuItem_Click);
            // 
            // tmpFoldersListbox
            // 
            this.tmpFoldersListbox.FormattingEnabled = true;
            this.tmpFoldersListbox.Location = new System.Drawing.Point(728, 28);
            this.tmpFoldersListbox.Name = "tmpFoldersListbox";
            this.tmpFoldersListbox.Size = new System.Drawing.Size(352, 147);
            this.tmpFoldersListbox.TabIndex = 14;
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(726, 450);
            this.Controls.Add(this.tmpFoldersListbox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.failedFilesListBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tmpFilesListbox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.foldersListBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.filesListBox);
            this.Controls.Add(this.folderButton);
            this.Controls.Add(this.fileButton);
            this.Name = "MainWindow";
            this.Text = "MainWindow";
            this.filesContextMenu.ResumeLayout(false);
            this.foldersContextMenu.ResumeLayout(false);
            this.tmpFilesContextMenuStrip.ResumeLayout(false);
            this.failedFilesContextMenu.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button fileButton;
        private System.Windows.Forms.Button folderButton;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ListBox filesListBox;
        private System.Windows.Forms.ContextMenuStrip filesContextMenu;
        private System.Windows.Forms.ToolStripMenuItem DeletionToolStripMenuItem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox foldersListBox;
        private System.Windows.Forms.ContextMenuStrip foldersContextMenu;
        private System.Windows.Forms.ToolStripMenuItem folderDeletionToolStripButton;
        private System.Windows.Forms.ToolStripMenuItem folderRemakeToolStripButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListBox tmpFilesListbox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ContextMenuStrip tmpFilesContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem добавитьФайлНаКонтрольЦелостностиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem игнорироватьДанныйФайлToolStripMenuItem;
        private System.Windows.Forms.ListBox failedFilesListBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ContextMenuStrip failedFilesContextMenu;
        private System.Windows.Forms.ToolStripMenuItem обновитьХешФайлаToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem удалитьФайлСКонтроляЦелостностиToolStripMenuItem;
        private System.Windows.Forms.ListBox tmpFoldersListbox;
    }
}