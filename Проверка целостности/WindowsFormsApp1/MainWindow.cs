﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

//using System.Security.Cryptography;

namespace WindowsFormsApp1
{
    public partial class MainWindow : Form
    {
        public MainWindow()
        {
            InitializeComponent();
            timer1.Interval = 10000; //таймер на 10 секунд
            timer1.Enabled = false; //поначалу не запущен
            this.openFileDialog1.Multiselect = true; //разрешаем выбирать несколько файлов при открытии файла из проводника

            // ассоциируем контекстное меню с текстовым полем
            filesListBox.ContextMenuStrip = filesContextMenu; //файлы на контроле целостности
            foldersListBox.ContextMenuStrip = foldersContextMenu; //папки на контроле целостности
            tmpFilesListbox.ContextMenuStrip = tmpFilesContextMenuStrip; //новые файлы в папках на контроле
            failedFilesListBox.ContextMenuStrip = failedFilesContextMenu; //файлы с проваленной проверкой целостности
        }

        //для работы с проводником(?)
        OpenFileDialog openFileDialog1 = new OpenFileDialog();

        //списки (на самом деле не очень нужны)
        List<string> toCheckFiles = new List<string>();
        List<string> toCheckFolders = new List<string>();
        List<string> filesHashes = new List<string>();
        List<string> filesToIgnore = new List<string>();
        List<string> failedFiles = new List<string>();

        //isFirst - переменная, нужная чтобы отличить что было добавлено при первоначальном добавлении папки (true), а что появилось после добавления на контроль (false)

        //кнопка для добавления одного файла на контроль
        private void button1_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.Cancel)
                return;
            string[] filename = openFileDialog1.FileNames; //получаем массив имён выделенных файлов

            foreach (string name in filename) //для каждого
            {
                ProcessFile(name, true); //обработать файл, первоначальное добавление файла/папки
            }
            timer1.Enabled = true; //запускаем таймер
        }

        //для работы с проводником у папок(?)
        FolderBrowserDialog folderBrowserDialog1 = new FolderBrowserDialog();

        //обработка папок и подпапок
        public void ProcessDirectory(string targetDirectory, bool isFirst)
        {
            if (toCheckFolders.Contains(targetDirectory)==false) //если данной папки нет в списке обрабатываемых
                toCheckFolders.Add(targetDirectory); //добавляем её в список обрабатываемых
            if (foldersListBox.Items.Contains(targetDirectory)==false) //то же самое с листбоксом папок
                foldersListBox.Items.Add(targetDirectory);
            // Process the list of files found in the directory. //получаем список файлов в папке
            string[] fileEntries = Directory.GetFiles(targetDirectory);
            foreach (string fileName in fileEntries) //для каждого фала
                ProcessFile(fileName, isFirst); //обработать файл

            // Recurse into subdirectories of this directory.
            string[] subdirectoryEntries = Directory.GetDirectories(targetDirectory); //получаем список подпапок
            foreach (string subdirectory in subdirectoryEntries) //для каждой подпапки
            {
                if (toCheckFolders.Contains(subdirectory)) //если подпапка есть в списке на контроле, то идём к следующей
                {
                    continue;
                }
                if (isFirst == false) //подпапка появилась после добавления папки на контроль
                {
                    //if (tmpFilesListbox.Items.Contains(subdirectory)==false)
                    //    tmpFilesListbox.Items.Add(subdirectory);
                    //ProcessDirectory(subdirectory, isFirst);
                    continue; //идём дальше
                }
                toCheckFolders.Add(subdirectory); //добавляем в список подпапку
                foldersListBox.Items.Add(subdirectory);
                ProcessDirectory(subdirectory, isFirst); //рекурсивно смотрим что в самой подпапке
            }
        }

        //обработка файла
        public void ProcessFile(string name, bool isFirst)
        {
            if (toCheckFiles.Contains(name)) //файл есть на контроле?
            {
                return; //да=скип
            }
            if (filesToIgnore.Contains(name)) //решили игнорить файл, появившийся в папке на контроле?
            {
                return; //да = скип (подумать так ли должно быть)
            }
            if (tmpFilesListbox.Items.Contains(name))
            {
                tmpFilesListbox.Items.Remove(name);
            }
            if (isFirst == true) //появился при изначальном добавлении
            {
                filesListBox.Items.Add(name); //добавляем файл в список на контроль
                string hash = Crypto.ReturninHash(name); //считаем хеш
                toCheckFiles.Add(name); //снова список файлов
                filesHashes.Add(hash); //список хешей
            }
            else //появился после добавления папки на контроль
            {
                //if (possibleToCheckFiles.Contains(name))
                if (tmpFilesListbox.Items.Contains(name)) //если такой файл уже есть в списке новых файлов
                {
                    return; //скипаем его
                }
                else 
                {
                    tmpFilesListbox.Items.Add(name); //добавялем файл в список файлов, появившихся после добавления папки на контроль
                }
            }
        }

        //удаление папки и подпапок
        public void ProcessDirectoryDeletion(string targetDirectory)
        {
            // Process the list of files found in the directory.
            string[] fileEntries = Directory.GetFiles(targetDirectory); //список файлов в папке
            foreach (string fileName in fileEntries)
                ProcessFileDeletion(fileName); //обрабатываем удаление

            // Recurse into subdirectories of this directory.
            string[] subdirectoryEntries = Directory.GetDirectories(targetDirectory); //получаем список подпапок
            foreach (string subdirectory in subdirectoryEntries)
            {
                int folderIndex = toCheckFolders.IndexOf(subdirectory); //получаем индекс в списке папок на проверку
                if (folderIndex == -1) //пытаемся удалить подпапку, которой нет на проверке
                    continue;
                toCheckFolders.RemoveAt(folderIndex); //удаляем по нему элемент списка на проверку папок
                foldersListBox.Items.Remove(subdirectory); //удаляем из листбокса первое вхождение такого названия
                ProcessDirectoryDeletion(subdirectory); //рекурсивно удаляем подпапки
            }
        }
        
        //удаление файла
        public void ProcessFileDeletion(string name)
        {
            int fileIndex = toCheckFiles.IndexOf(name); //находим индекс файла на удаление
            if (fileIndex == -1) //при попытке удалить файл, которого нет в списке - пропускаем
                return;
            toCheckFiles.RemoveAt(fileIndex); //по индексу удаляем из списка файлов на проверку
            filesHashes.RemoveAt(fileIndex); //по тому же индексу(!!) удаляем хеш из списка хешей
            filesListBox.Items.Remove(name); //удаляем первое вхождение такого файла из списка файлов на проверку (на форме)
        }

        //добавление папки на контроль
        private void button2_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                string pathToDirectory = folderBrowserDialog1.SelectedPath;
                if (toCheckFolders.Contains(pathToDirectory) == false)
                {
                    //toCheckFolders.Add(pathToDirectory);
                    ProcessDirectory(pathToDirectory, true); //метод по обработке папки
                    //foldersListBox.Items.Add(pathToDirectory);
                } 
            }
            timer1.Enabled = true; //запуск таймера
        }

        Crypto operations = new Crypto();

        //проверка на целостность каждые 10 секунд
        private void timer1_Tick(object sender, EventArgs e)
        {
            if (toCheckFiles.Count == 0 && toCheckFolders.Count==0) //список файлов на проверку пуст
            {
                timer1.Stop(); //осткавнавливаем таймер
                filesListBox.Items.Clear(); //очищаем списки на проверке нет и не может быть абсолютно ничего
                foldersListBox.Items.Clear();
                tmpFilesListbox.Items.Clear();
                failedFilesListBox.Items.Clear();
                toCheckFiles.Clear();
                toCheckFolders.Clear();
                filesHashes.Clear();
                filesToIgnore.Clear();
                failedFiles.Clear();
                return;
            }
            for (int folder = 0; folder < toCheckFolders.Count; folder++) //для каждой папки в списке папок на проверку
            {
                ProcessDirectory(toCheckFolders[folder], false); //добавляем файлы, которые появились после добавления папок на контроль в список новых файлов
                
            /*
                DirectoryInfo dI = new DirectoryInfo(toCheckFolders[folder]);
                FileInfo[] allFiles = dI.GetFiles();
                foreach (FileInfo fi in allFiles)
                {
                    if (toCheckFiles.Contains(toCheckFolders[folder] + '\\' + fi.Name))
                    {
                        continue;
                    }
                    // выводим этот файл
                    string hash = Crypto.ReturninHash(toCheckFolders[folder] + '\\' + fi.Name);
                    toCheckFiles.Add(toCheckFolders[folder] + '\\' + fi.Name);

                    filesListBox.Items.Add(toCheckFolders[folder] + '\\' + fi.Name);
                    filesHashes.Add(hash);
                }
                */
            }
            
            for (int i = 0; i < toCheckFiles.Count; i++) //для каждого файла в списке файлов на проверку
            {
                if (Crypto.ReturninHash(toCheckFiles[i]) == filesHashes[i]) //сравнить заново посчитанный хеш и хеш в ячейке с тем же индексом что и индекс файла в списке файлов на проверку (!!)
                {
                    //MessageBox.Show("Проверка пройдена!");
                }
                else
                {
                    MessageBox.Show("Ошибка целостности!!!!");
                    if (failedFiles.Contains(toCheckFiles[i])) //если файл уже находится в списке файлов, с проваленной проверкой целостности то скип
                    { 
                        continue;
                    }
                    failedFiles.Add(toCheckFiles[i]); //инчае добавляем этот файл в список файлов с повреждённой целостностью
                    failedFilesListBox.Items.Add(toCheckFiles[i]); //и на форму тоже
                }
            }
        }

        //кнопка для удаления файлов с контроля из списков файлов
        private void jjToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (string s in filesListBox.SelectedItems.OfType<string>().ToList()) //каждый выдел элемент
            {
                int fileIndex = toCheckFiles.IndexOf(s); //находим индекс по названию
                toCheckFiles.RemoveAt(fileIndex); //удаляем из списка по индексу элемента
                filesHashes.RemoveAt(fileIndex); //удаляем из списка хешей по тому же индексу (!!)
                filesListBox.Items.Remove(s); //и с формы тоже
                if (failedFiles.Contains(s)) //если этот файл есть в списке файлов с ошибкой целостности
                {
                    int failIndex = failedFiles.IndexOf(s); //находим по названию файла индекс списка файлов с ошибкой целостности
                    failedFiles.RemoveAt(failIndex); //удаляем файл из списка файлов с нарушенной целостностью
                    failedFilesListBox.Items.RemoveAt(failIndex); //и с формы тоже
                }
            }
        }

        /*
        private void обновитьХешФайлаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (string s in filesListBox.SelectedItems.OfType<string>().ToList())
            {
                int fileIndex = toCheckFiles.IndexOf(s);
                //filesHashes.RemoveAt(fileIndex);
                string hash = Crypto.ReturninHash(s);
                //filesHashes.Add(hash);
                filesHashes.Insert(fileIndex, hash);
            }
        }
        */
        
        //кнопка для удаления папок с контроля целостности
        private void удалитьВыделенныеФайлыСКонтроляЦелостностиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (string folder in foldersListBox.SelectedItems.OfType<string>().ToList()) //для каждого выд элемента
            {
                int folderIndex = toCheckFolders.IndexOf(folder); //по названию находим индекс в списке папок
                ProcessDirectoryDeletion(folder); //вызываем метод удаления папок
                if (folderIndex == -1)
                    continue;
                toCheckFolders.RemoveAt(folderIndex); //удаляем папку из списка папок на проверку
                foldersListBox.Items.Remove(folder); //и с формы тоже
            }
        }
        
        //кнопка для обновления хешей всех файлов в папке 
        private void обновитьХешФайловВПапкеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            /*
            foreach (string s in foldersListBox.SelectedItems.OfType<string>().ToList()) //для каждой выделенной папки
            {
               ProcessDirectoryDeletion(s);
               ProcessDirectory(s, true);
            }
            */
            
            foreach (string folder in foldersListBox.SelectedItems.OfType<string>().ToList())
            {
                DirectoryInfo dI = new DirectoryInfo(folder);
                FileInfo[] allFiles = dI.GetFiles();
                foreach (FileInfo fi in allFiles)
                {
                    if (toCheckFiles.Contains(folder + '\\' + fi.Name) == false)
                        continue;
                    //string hash = Crypto.ReturninHash(folder + '\\' + fi.Name);
                    //toCheckFiles.Add(folder + '\\' + fi.Name);
                    //filesListBox.Items.Add(folder + '\\' + fi.Name);
                    int fileIndex = toCheckFiles.IndexOf(folder + '\\' + fi.Name); //ищем по названию индекс в списке файлов на проверкку
                    //filesHashes.RemoveAt(fileIndex); //удалить хеш по тому же индексу (!!)
                    
                    string hash = Crypto.ReturninHash(folder + '\\' + fi.Name); //рассчитать хеш 
                    filesHashes[fileIndex] = hash;
                    //filesHashes.Add(hash); //добавить хеш в список хешей
                    if (failedFiles.Contains(folder + '\\' + fi.Name) == false)
                        continue;
                    failedFiles.RemoveAt(failedFiles.IndexOf(folder + '\\' + fi.Name)); //удалить из списка файлов индекс, найденный по имени
                    failedFilesListBox.Items.Remove(folder + '\\' + fi.Name); //и с формы
                    filesHashes.Add(hash);
                    if (failedFilesListBox.Items.Contains(folder + '\\' + fi.Name))
                    {
                        failedFilesListBox.Items.Remove(folder + '\\' + fi.Name);
                        failedFiles.Remove(folder + '\\' + fi.Name);
                    }

                }
            }
            
        }

        //Form formAdditionalInfo = new AdditionalInfo();
      
        //кнопка для игнорирования нового файла в папке (после того как её поставили на контроль)
        private void игнорироватьДанныйФайлToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (string s in tmpFilesListbox.SelectedItems.OfType<string>().ToList()) //для каждого выделенного файла
            {
                filesToIgnore.Add(s); //добавляем в список на игнор
                //ProcessFile(s, true);
                tmpFilesListbox.Items.Remove(s); //убираем файл с формы
            }
        }
        //кнопка для добавления нового файла на контроль (после того как папку поставили на контроль)
        private void добавитьФайлНаКонтрольЦелостностиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (string s in tmpFilesListbox.SelectedItems.OfType<string>().ToList()) // для каждого выделенного файла
            {
                ProcessFile(s, true); //обработать файл как добавленный на контроль руками
                tmpFilesListbox.Items.Remove(s); //убрать с формы
            }
        }

        //кнопка для удаления файла с нарушенной целостностью с контроля целостности
        private void удалитьФайлСКонтроляЦелостностиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (string s in failedFilesListBox.SelectedItems.OfType<string>().ToList()) //для всех выдел
            {
                int fileIndex = toCheckFiles.IndexOf(s); //ищем индекс списка файлов на проверку по названию
                toCheckFiles.RemoveAt(fileIndex); //удаляем этот элемент
                filesHashes.RemoveAt(fileIndex); //удаляем элемент по этому же индексу из списка хешей (!!)
                filesListBox.Items.Remove(s); //и с формы
                failedFilesListBox.Items.Remove(s); //и снова с формы
                failedFiles.RemoveAt(failedFiles.IndexOf(s)); //и из списка файлов с проваленной проверкой
            }
        }

        //кнопка для обновления хеша с нарушенной целостностью 
        private void обновитьХешФайлаToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            foreach (string s in failedFilesListBox.SelectedItems.OfType<string>().ToList()) //для каждого выдел
            {
                int fileIndex = toCheckFiles.IndexOf(s); //ищем по названию индекс в списке файлов на проверкку
                //filesHashes.RemoveAt(fileIndex); //удалить хеш по тому же индексу (!!)
                string hash = Crypto.ReturninHash(s); //рассчитать хеш 
                filesHashes[fileIndex] = hash;
                //filesHashes.Add(hash); //добавить хеш в список хешей
                failedFiles.RemoveAt(failedFiles.IndexOf(s)); //удалить из списка файлов индекс, найденный по имени
                failedFilesListBox.Items.Remove(s); //и с формы
            }
        }
    }
}
